This is an early prototype for a PLUTO editor.

To get started, clone the repository and install dependencies:

```
$ git clone
$ cd pluto-editor
$ virtualenv venv
$ source venv\bin\activate
$ pip install -r requirements.txt
```

To start editor:

```
$ python editor.py
```

To see the parsed terminals:

```
$ python terminals.py
```
