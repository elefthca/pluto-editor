from lark import Lark


GRAMMAR_FILE = input('Grammar file: ')

with open(GRAMMAR_FILE, 'r') as f:
    grammar = f.read()

lark = Lark(grammar, parser=None, lexer='standard')

with open(GRAMMAR_FILE + '.txt', 'w') as f:
    for tdef in lark.lexer.terminals:
        name = tdef.name
        pattern = str(tdef.pattern)
        line = ': '.join([name, pattern]) + '\n'
        f.write(line)
